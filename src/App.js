import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';
import Navbar from './components/AppNavBar';
import Landing from './pages/Home';
//import Foot from './components/Footer' //activity
import Products from './pages/Catalog';
import Login from './pages/Login';
import Signup from './pages/Register';
//import Error from './pages/Error';

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
//clean the state of the entry point 
export default function App() {
  
  return (

  	<Router>
    	<Navbar />

      {<Switch>
        <Route exact path="/" component={Landing} />
        <Route exact path="/products" component={Products} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Signup} />
        {/*<Route component={Error} />*/}
      </Switch>}

      {/*<Foot />*/}
    </Router>
    
    );
}

