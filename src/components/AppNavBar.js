import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import { NavLink } from 'react-router-dom';

export default function AppNavbar() {
	return(
		<Navbar bg = "light" expand = "lg">
			<Navbar.Brand>
				SHOPPING STORE
			</Navbar.Brand>
			<Navbar.Toggle aria-controls = "basic-navbar-nav" />
			<Navbar.Collapse id ="basic-navbar-nav">
				<Nav.Link as={NavLink} to="/">Dashboard</Nav.Link>
				<Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
				<Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
				<Nav.Link as={NavLink} to="/products" >Products</Nav.Link>
			</Navbar.Collapse>
		</Navbar>
		)
}
