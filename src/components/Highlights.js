import { Row, Col, Card as Baraha } from 'react-bootstrap';

export default function Highlights() {
	return(
		<Row className="mt-3 mb-3">
			{/*1st Column*/}
			<Col xs={12} md={4}>
				<Baraha className="BarahaHighlight p-3">
					<Baraha.Body>
						<Baraha.Title>
							Learn From Home
						</Baraha.Title>
						<Baraha.Text>
							Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Sequi excepturi quaerat nostrum, ullam laboriosam dignissimos aperiam magnam quis, minima similique.
					<	/Baraha.Text>
					</Baraha.Body>
				</Baraha>
			</Col>
			{/*2nd Column*/}
			<Col xs={12} md={4}>
				<Baraha className="BarahaHighlight p-3">
					<Baraha.Body>
						<Baraha.Title>
							Study Now, Pay Later
						</Baraha.Title>
						<Baraha.Text>
							Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Nostrum voluptatem doloremque eum, deleniti explicabo facere nemo quo alias assumenda nam.
						</Baraha.Text>
					</Baraha.Body>
				</Baraha>
			</Col>
			{/*3rd Column*/}
			<Col xs={12} md={4}>
				<Baraha className="BarahaHighlight p-3">
					<Baraha.Body>
						<Baraha.Title>
							Be Part of our community
						</Baraha.Title>
						<Baraha.Text>
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nihil vero repellendus porro dolorum officiis quos, iure. Quam tempora, laudantium veniam.
						</Baraha.Text>
					</Baraha.Body>
				</Baraha>
			</Col>
		</Row>
		);
}