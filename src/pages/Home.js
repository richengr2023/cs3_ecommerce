//import Hero from '../components/Banner';
import Showcase from '../components/Highlights';
import Container from 'react-bootstrap/Container';
//import Foot from '../components/Footer';

let details = {
	title: "You're at the Home Page",
	tagline: "Pag luto na ang kanin pwede na kong ulamin",
	label: "Enroll Now!"
}

export default function Home() {
	return(
		<Container>
			{/*<Hero kahitAno={details} />*/}
			<Showcase />
			{/*<Foot />*/}
		</Container>
	)
}
